import cv2
from sewar.full_ref import mse, rmse, psnr, uqi, ssim, ergas, scc, rase, sam, msssim, vifp

img_label = [
    'original_solution_pop_10_20000.jpeg',

    'solution_pop_5_20000_solution_gen_20000.png',
    'solution_pop_15_20000_solution_gen_20000.png',
    'solution_pop_30_20000_solution_gen_20000.png',
    'solution_pop_45_20000_solution_gen_20000.png',
    'solution_pop_60_20000_solution_gen_20000.png',

    'solution_pop_5_30000_solution_gen_30000.png',
    'solution_pop_15_30000_solution_gen_30000.png',
    'solution_pop_30_30000_solution_gen_30000.png',
    'solution_pop_45_30000_solution_gen_30000.png',
    'solution_pop_60_30000_solution_gen_30000.png',

    'solution_pop_5_40000_solution_gen_40000.png',
    'solution_pop_15_40000_solution_gen_40000.png',
    'solution_pop_30_40000_solution_gen_40000.png',
    'solution_pop_45_40000_solution_gen_40000.png',
    'solution_pop_60_40000_solution_gen_40000.png',
    ]

#images path
base_img = r'../fruit.jpg'
for i in range(len(img_label)):
    comp_img = r'../outputSolutions/'+img_label[i]

    #read images
    original = cv2.imread(base_img)
    contrast = cv2.imread(comp_img)

    #similarity comparation method calc
    img_similarity = [mse(original,contrast), rmse(original,contrast), psnr(original,contrast)]
    print(img_similarity)
